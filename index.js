console.log("Hellow World")

let num = prompt("Enter number: ")
let getCube = Math.pow(num,3);
console.log(`The cube of ${num} is: ${getCube}`);


const address = [1553,"P ubalde St.","22-A","Gingoog City","Misamis Oriental", 9014];

const [houseNumber, Street, Barangay, Municipality, Province, Zipcode] = address;

console.log(` I live at ${houseNumber} ${Street}, ${Barangay}, ${Municipality}, ${Province},${Zipcode}`);

const animal = {
	name: "lolong",
	type: "Salt water crocodile",
	measurement: 1075,
	kg: "20 ft 3 in"
}

let animalName = animal.name;
let animalType = animal.type;
let animalMeasurement = animal.measurement;
let animalkg = animal.kg;

console.log(`${animalName} was a ${animalType}. He weighed at ${animalMeasurement} kgs with a measurement of ${animalkg}`);





const numbers = [1,2,3,4,5];

numbers.forEach((num) => console.log(num))

let reduceNumber = numbers.reduce((a,b)=> a+b);
console.log(reduceNumber);

class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog = new Dog("Spliff",5,"doberman");
console.log(dog);